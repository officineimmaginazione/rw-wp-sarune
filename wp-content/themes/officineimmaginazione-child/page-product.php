<?php
/*
Template Name: Pagina prodotto
*/
?>

<?php while (have_posts()) : the_post(); ?>
	<?php 
	
	$prefix = "pl_";
	$left = array (
		"overtitle" => get_post_meta(get_the_ID(), $prefix . "overtitle", true),
		"title" => get_post_meta(get_the_ID(), $prefix . "title", true),
		"content" => get_post_meta(get_the_ID(), $prefix . "content", true),
	);
	
	//print_r($extra_featured_image); exit;

	$prefix = "pr_";
	$right = array (
		"title" => get_post_meta(get_the_ID(), $prefix . "title", true),
		"subtitle" => get_post_meta(get_the_ID(), $prefix . "subtitle", true),
		"icon_list" => array (
			array(
				"icon" => get_post_meta(get_the_ID(), $prefix . "icon_first", true)["url"],
				"content" => get_post_meta(get_the_ID(), $prefix . "content_first", true),
			),
			array(
				"icon" => get_post_meta(get_the_ID(), $prefix . "icon_second", true)["url"],
				"content" => get_post_meta(get_the_ID(), $prefix . "content_second", true),
			),
			array(
				"icon" => get_post_meta(get_the_ID(), $prefix . "icon_third", true)["url"],
				"content" => get_post_meta(get_the_ID(), $prefix . "content_third", true),
			),
			array(
				"icon" => get_post_meta(get_the_ID(), $prefix . "icon_fourth", true)["url"],
				"content" => get_post_meta(get_the_ID(), $prefix . "content_fourth", true),
			),
			array(
				"icon" => get_post_meta(get_the_ID(), $prefix . "icon_fifth", true)["url"],
				"content" => get_post_meta(get_the_ID(), $prefix . "content_fifth", true),
			),
			array(
				"icon" => get_post_meta(get_the_ID(), $prefix . "icon_sixth", true)["url"],
				"content" => get_post_meta(get_the_ID(), $prefix . "content_sixth", true),
			),
		),
		"link" => array(
			"txt" => get_post_meta(get_the_ID(), $prefix . "link_txt", true),
			"url" => get_post_meta(get_the_ID(), $prefix . "link_url", true)
		),
		"btn" => array(
			"txt" => get_post_meta(get_the_ID(), $prefix . "btn_txt", true),
			"url" => get_post_meta(get_the_ID(), $prefix . "btn_url", true)
		),
	);
	
	
	?>
	<main class="product-main">
		<div class="container-fluid align-items-center justify-content-center main-body">
			<?php if(has_post_thumbnail()) : ?>
				<div class="featured-img-container">
					<img class="circle" src="<?php echo get_stylesheet_directory_uri() . "/assets/img/circle.png"; ?>" alt="" />
					<img class="featured-img" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" />
				</div>
			<?php endif; ?> 
      <div class="row">
				<div class="d-flex flex-wrap align-items-center col-12 col-lg-6 order-1 order-lg-0 main-body-side body-side-left">
					<div class="side-body">
						<?php if(array_filter($left)) : ?>
							<?php if($left["overtitle"]) : ?>
								<h5 class="side-subtitle"><?php echo $left["overtitle"]; ?></h5>
							<?php endif; ?>
							<?php if($left["title"]) : ?>
								<h2 class="side-title"><?php echo $left["title"]; ?></h2>
							<?php endif; ?>
							<?php if($left["content"]) : ?>
								<div class="side-content"><?php echo $left["content"]; ?></div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
        <div class="d-flex flex-wrap align-items-center col-12 col-lg-6 order-0 order-lg-1 main-body-side body-side-right">
					<div class="side-body">
						<?php if(array_filter($right)) : ?>
							<?php if($right["title"]) : ?>
								<h1 class="side-title"><?php echo $right["title"]; ?></h1>
							<?php endif; ?>
							<?php if($right["subtitle"]) : ?>
								<h5 class="side-subtitle"><?php echo $right["subtitle"]; ?></h5>
							<?php endif; ?>
							<?php if(array_filter($right["icon_list"])) : ?>
								<div class="row">
									<?php foreach($right["icon_list"] as $row) : ?>
										<div class="d-flex align-items-center w-100 mb-2">
											<?php if($row["icon"]) : ?>
												<div class="icon-box">
													<img class="img-fluid" src="<?php echo $row["icon"] ?>" alt="" />
												</div>
											<?php endif; ?>
											<?php if($row["content"]) : ?>
												<div class="pl-2">
													<?php echo $row["content"]; ?>
												</div>
											<?php endif; ?>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							<?php if(array_filter($right["link"])) : ?>
								<div class="w-100 text-center mt-3">
									<a class="btn btn-link" href="<?php echo $right["link"]["url"]; ?>" >
										<i class="la la-long-arrow-right"></i>
										<span><?php echo $right["link"]["txt"]; ?></span>
									</a>
								</div>
							<?php endif; ?>
							<?php if(array_filter($right["btn"])) : ?>
								<div class="w-100 text-center mt-3">
									<a class="btn btn-primary" href="<?php echo $right["btn"]["url"]; ?>" >
										<?php echo $right["btn"]["txt"]; ?>
									</a>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
        </div>
      </div>
    </div>
	</main>
<?php endwhile; ?>
<script>
    $(document).ready(function(){
      
			function assignHeight(target) {
				win = $(window);
				header = $(".header");
				footer = $(".footer");
				if(win.outerHeight() > 700 && win.outerWidth() > 991) {
					$(target).css("height", ((win.height() - header.height())));
				}
				else {
					/*if ((win.height() > 700 && win.width() < 768)) {
						$(target).css("height", (win.height() - (header.outerHeight() + footer.outerHeight())));
					}*/
				}
			}
			
			function assignMaxHeight(target) {
				win = $(window);
				header = $(".header");
				footer = $(".footer");
				$(target).css("max-height", (win.height() - (header.outerHeight() + footer.outerHeight() + 100)));
			}
			
			assignHeight(".main-body-side");
			$(window).resize(function() {
				assignHeight(".main-body-side");
			});
			
			var currentMousePos = { x: -1, y: -1 };
			$(document).mousemove(function(event) {
				if ($(window).width() > 768){
					
					var $wWidth = $(window).innerWidth();
					var $wHeight = $(window).innerHeight();
					var centerX = $wWidth / 2;
					var centerY = $wHeight / 2;
					
					xBgPosition = ((centerX - event.pageX) / 150);
					yBgPosition = ((centerY - event.pageY) / 150);
					$(".side-img").css('background-position', 'calc(50% - ' + xBgPosition + 'px) calc(50% - ' + yBgPosition + 'px)');
					
				}
			});
			
			
    });
</script>