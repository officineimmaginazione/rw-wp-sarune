<?php $catid = get_query_var("cat"); ?>

<main class="category-main">
	<div class="container">
		<h1 class="category-title"><?php echo get_cat_name($catid); ?></h1>
		<?php if (have_posts()) : ?>
			<div class="row">
				<?php while (have_posts()) : the_post(); ?>
					<div class="col-12 col-md-6 col-lg-4 mb-5">
						<article <?php post_class("d-flex flex-wrap flex-column justify-content-start post-preview"); ?> id="post-<?php the_ID(); ?>">
							<header class="post-preview-header mb-0">
								<div class="post-preview-date"><?php echo get_the_date("d F Y"); ?></div>
								<div class="post-image"><?php echo get_tag_picture(get_post_thumbnail_id()); ?></div>
							</header>
							<section class="post-preview-body mb-auto">
								<h3 class="post-preview-title"><?php the_title(); ?></h3>
								<div class="post-preview-excerpt">
									<?php echo wp_trim_words(get_the_content(), 30); ?>
								</div>
							</section>
							<footer class="post-preview-footer mt-auto mb-0">
								<a class="btn btn-link" href="<?php the_permalink() ?>">
									<i class="la la-long-arrow-right"></i>
									<span><?php _e("Leggi di più", "officineimmaginazione"); ?></span>
								</a>
							</footer>
						</article>
					</div>
				<?php endwhile; ?>
			</div>
		<?php else: ?>
			<p><?php _e("Nessun risultato trovato", "officineimmaginazione"); ?></p>
		<?php endif; ?>
		<div><?php oi_pagination(); ?></div>
	</div>
</main>