<?php
/**
 * Scripts and stylesheets
 *
 */
function oi_scripts() {
	/**
	 * The build task in Grunt renames production assets with a hash
	 * Read the asset names from assets-manifest.json
	 */
	$assets = array(
		'bootstrap_css'		=> '/assets/css/bootstrap.min.css',
		'fontawesome_css'   => '/assets/css/fontawesome.min.css',
		'lineawesome_css'   => '/assets/css/line-awesome.min.css',
		'jssocials_css'		=> '/assets/css/jssocials.min.css',
		'scrollbar_css'		=> '/assets/css/jquery.mCustomScrollbar.min.css',
		'oi_css'            => '/assets/css/style.min.css',
		'jquery'			=> '/assets/js/jquery.min.js',
		'popper_js'         => '/assets/js/popper.min.js', 
    'bootstrap_js'      => '/assets/js/bootstrap.min.js',
		'jssocials_js'		=> '/assets/js/jssocials.min.js',
		'scrollbar_js'		=> '/assets/js/jquery.mCustomScrollbar.concat.min.js',
	);
	wp_enqueue_style('bootstrap_css', get_template_directory_uri() . $assets['bootstrap_css'], false, null);
	wp_enqueue_style('fontawesome_css', get_stylesheet_directory_uri() . $assets['fontawesome_css'], false, null);
	wp_enqueue_style('lineawesome_css', get_stylesheet_directory_uri() . $assets['lineawesome_css'], false, null);
	wp_enqueue_style('oi_css', get_template_directory_uri() . $assets['oi_css'], false, null);
    wp_enqueue_style('jssocials_css', get_stylesheet_directory_uri() . $assets['jssocials_css'], false, null);
	wp_enqueue_style('scrollbar_css', get_stylesheet_directory_uri() . $assets['scrollbar_css'], false, null);
	wp_enqueue_style('oi_child_style', get_stylesheet_directory_uri() . $assets['oi_css'], 'oi_css', wp_get_theme()->get('Version'));
    wp_deregister_script('jquery');
	wp_enqueue_script('jquery', get_template_directory_uri() . $assets['jquery'], array(), null, false);
    wp_enqueue_script('popper_js', get_template_directory_uri() . $assets['popper_js'], array(), null, true);
	wp_enqueue_script('bootstrap_js', get_template_directory_uri() . $assets['bootstrap_js'], array(), null, true);
	wp_enqueue_script('jssocials_js', get_stylesheet_directory_uri() . $assets['jssocials_js'], array(), null, true);
	wp_enqueue_script('scrollbar_js', get_stylesheet_directory_uri() . $assets['scrollbar_js'], array(), null, true);
}
add_action('wp_enqueue_scripts', 'oi_scripts', 100);

function oi_admin_theme_style() {
	$adminassets = array (
        'admin_css'	=> '/assets/css/admin.css',
		'jquery'	=> '/assets/js/jquery.min.js'
	);
    wp_enqueue_style('admin_css', get_stylesheet_directory_uri() . $adminassets['admin_css'], false, null);
	wp_enqueue_script('jquery');
	/* Add Image Upload to Series Taxonomy */
	if (!did_action('wp_enqueue_media')){
		wp_enqueue_media();
	}
}
add_action('admin_enqueue_scripts', 'oi_admin_theme_style');
add_action('login_enqueue_scripts', 'oi_admin_theme_style');