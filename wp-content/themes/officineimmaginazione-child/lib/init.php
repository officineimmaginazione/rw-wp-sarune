<?php
/**
 * Responsabile initial setup and constants
 */
function oi_setup() {
	load_theme_textdomain ('officineimmaginazione', get_template_directory() . '/lang');
	register_nav_menus (array(
		'nav_left' => __('Menu sinistro', 'officineimmaginazione'),
		'nav_right' => __('Menu destro', 'officineimmaginazione')
	));
	add_editor_style('style.css');
}
add_action('after_setup_theme', 'oi_setup');

/**
 * Register sidebars
 */
function oi_widgets_init() {
	register_sidebar(array(
		'name'			=> __('Piè di pagina - Lato sinistro', 'officineimmaginazione'),
		'id'			=> 'footer-left',
		'before_widget'	=> '<div class="%1$s %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<strong>',
		'after_title'	=> '</strong>',
	));
	register_sidebar(array(
		'name'			=> __('Piè di pagina - Lato destro', 'officineimmaginazione'),
		'id'			=> 'footer-right',
		'before_widget'	=> '<div class="%1$s %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<strong>',
		'after_title'	=> '</strong>',
	));
}
add_action('widgets_init', 'oi_widgets_init');
add_theme_support( 'post-thumbnails' ); 