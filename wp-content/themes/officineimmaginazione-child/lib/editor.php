<?php

/*
 * Creates a custom colour palette
 */
function dmw_custom_palette( $init ) { 
  $custom_colours = '"000000", "Nero", "ffffff", "Bianco", "d16e43", "Red Mask", "f1ede6", "Romance", "757575", "Grey"'; 
  $init['textcolor_map'] = '['.$custom_colours.']'; 
  return $init; 
} 
add_filter('tiny_mce_before_init', 'dmw_custom_palette'); 