<?php
/*
  Plugin Name: Officine Immaginazione Social Profile Widget
  Plugin URI: http://www.officineimmaginazione.com
  Description: 
  Author: Officine Immaginazione
  Author URI: https://www.officineimmaginazione.com
*/

class OI_Social_Profile extends WP_Widget {
	
	function __construct() {
		parent::__construct(
			'OI_Social_Profile',
			__('Profili Social', 'officineimmaginazione'),
			array ('description' => __('Link ai vari profili social dell\'utente', 'officineimmaginazione'))
		);
	}
	
	public function form($instance) {
        isset($instance['facebook']) ? $facebook = $instance['facebook'] : null;
        isset($instance['google']) ? $google = $instance['google'] : null;
		isset($instance['youtube']) ? $youtube = $instance['youtube'] : null;
        isset($instance['instagram']) ? $instagram = $instance['instagram'] : null;
        isset($instance['twitter']) ? $twitter = $instance['twitter'] : null;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('facebook'); ?>"><?php _e('Facebook:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" type="text" value="<?php echo esc_attr($facebook); ?>">
        </p>
				<p>
            <label for="<?php echo $this->get_field_id('youtube'); ?>"><?php _e('Youtube:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('youtube'); ?>" name="<?php echo $this->get_field_name('youtube'); ?>" type="text" value="<?php echo esc_attr($youtube); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('instagram'); ?>"><?php _e('Instagram:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('instagram'); ?>" name="<?php echo $this->get_field_name('instagram'); ?>" type="text" value="<?php echo esc_attr($instagram); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('twitter'); ?>"><?php _e('Twitter:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" type="text" value="<?php echo esc_attr($twitter); ?>">
        </p>
        <?php
    }
	
	public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['facebook'] = (!empty($new_instance['facebook']) ) ? strip_tags($new_instance['facebook']) : '';
		$instance['youtube'] = (!empty($new_instance['youtube']) ) ? strip_tags($new_instance['youtube']) : '';
        $instance['instagram'] = (!empty($new_instance['instagram']) ) ? strip_tags($new_instance['instagram']) : '';
        $instance['twitter'] = (!empty($new_instance['twitter']) ) ? strip_tags($new_instance['twitter']) : '';
        return $instance;
    }
	
	public function widget($args, $instance) {
		$facebook = $instance['facebook'];
		$youtube = $instance['youtube'];
        $instagram = $instance['instagram'];
        $twitter = $instance['twitter'];

        $facebook_profile = '<a href="'.$facebook.'" target="_blank"><i class="fab fa-facebook"></i></a>';
        $youtube_profile = '<a href="'.$youtube.'" target="_blank"><i class="fab fa-youtube"></i></a>';
        $instagram_profile = '<a href="'.$instagram.'" target="_blank"><i class="fab fa-instagram"></i></a>';
        $twitter_profile = '<a href="'.$twitter.'" target="_blank"><i class="fab fa-twitter"></i></a>';
        
		echo $args['before_widget'];
		echo '<div class="social-icons">';
		echo (!empty($facebook)) ? $facebook_profile : null; 
		echo (!empty($youtube)) ? $youtube_profile : null;
		echo (!empty($instagram)) ? $instagram_profile : null;
    echo (!empty($twitter)) ? $twitter_profile : null;
		echo '</div>';
		echo $args['after_widget'];
	}
}