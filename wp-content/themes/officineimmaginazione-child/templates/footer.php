<footer class="d-flex flex-wrap align-items-center w-100 footer">
	<div class="d-flex flex-wrap align-items-center order-md-1 ml-md-auto footer-right">
		<?php dynamic_sidebar("footer-right"); ?>
	</div>
	<div class="d-flex flex-wrap align-items-center footer-left">
		<?php dynamic_sidebar("footer-left"); ?>
	</div>
</footer>

<script>
	jQuery(document).ready(function($) {
		
	});
</script>