<header class="header">
	<nav class="navbar navbar-expand-md">
		<div class="d-flex w-100 d-md-none">
			<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
				<img class="img-fluid img-logo-xs" src="<?php echo get_stylesheet_directory_uri() . "/assets/img/logo.svg"; ?>" alt="" />
			</a>
			<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fas fa-bars"></i>
			</button>
		</div>
		<div class="collapse navbar-collapse" id="navbarToggler">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-md px-0">
						<?php
						if (has_nav_menu('nav_left')) {
							wp_nav_menu(array(
								'theme_location' => 'nav_left', 
								'menu_class' => 'nav navbar-nav nav-fill navbar-menu',
								'walker' => new oi_Nav_Walker()
							));
						}
						?>
					</div>
					<div class="d-none d-md-block col-md-auto px-0 text-center">
						<a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
							<img class="img-fluid img-logo-xs" src="<?php echo get_stylesheet_directory_uri() . "/assets/img/logo.svg"; ?>" alt="" />
							<span class="brand-tagline"><?php echo get_bloginfo("description"); ?></span>
						</a>
					</div>
					<div class="col-12 col-md px-0">
						<?php
						if (has_nav_menu('nav_right')) {
							wp_nav_menu(array(
								'theme_location' => 'nav_right', 
								'menu_class' => 'nav navbar-nav navbar-right nav-fill navbar-menu',
								'walker' => new oi_Nav_Walker()
							));
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</nav>
</header>