<?php
/*
Theme Name: Officine Immaginazione
Theme URI: http://www.officineimmaginazione.com/wordpress-themes
Description: OfficineImmaginazione is a Wordpress theme based on Bootstrap, FontAwesome, ecc.
Version: 1.0.0
Author: Officine Immaginazione
Author URI: http://www.officineimmaginazione.com
License: All right is reserved
*/

$oi_includes = array(
	'lib/init.php',				
	'lib/wrapper.php',			
	'lib/sidebar.php',			
	'lib/config.php',			
	'lib/activation.php',		
	'lib/nav.php',				
	'lib/scripts.php',			
	'lib/titles.php',			
	'lib/pagination.php',
  'lib/breadcrumbs.php',
	'lib/security.php',			
	'lib/editor.php',			
  'lib/utils.php',
  'lib/widget.php',
	'metaboxes/homepage.php',
	'metaboxes/page-institutional.php',
	'metaboxes/page-map.php',
	'metaboxes/page-product.php',
	'metaboxes/page-contacts.php'
);

foreach ($oi_includes as $file) {
	if (!$filepath = locate_template($file)) 
		trigger_error(sprintf(__('Error locating %s for inclusion', 'roots'), $file), E_USER_ERROR);
	require_once $filepath;
}
unset($file, $filepath);

remove_filter('the_content', 'wptexturize');


add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );

/* Disable WordPress Admin Bar for all users but admins. */
show_admin_bar(false);


/* Flush rewrite rules for custom post types on permalink generation. */
//flush_rewrite_rules(true);
//add_action('after_switch_theme', 'flush_rewrite_rules');

//add_filter('upload_mimes', 'cc_mime_types');

add_filter('intermediate_image_sizes_advanced', 'prefix_remove_default_images');

$to_add = [
    array("xs", 575, 9999),
    array("sm", 767, 9999),
    array("md", 991, 9999),
    array("lg", 1199, 9999)
];
add_action('after_setup_theme', 'set_image_sizes', 11);
do_action('after_setup_theme', $to_add);

add_action('admin_init', 'hideEditor');


function register_oi_social_profile() {
	register_widget('OI_Social_Profile');
}
add_action('widgets_init', 'register_oi_social_profile');

/* @Recreate the default filters on the_content so we can pull formated content with get_post_meta
-------------------------------------------------------------- */
add_filter( 'meta_content', 'wptexturize'        );
add_filter( 'meta_content', 'convert_smilies'    );
add_filter( 'meta_content', 'convert_chars'      );
add_filter( 'meta_content', 'wpautop'            );
add_filter( 'meta_content', 'shortcode_unautop'  );
add_filter( 'meta_content', 'prepend_attachment' );


add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;

  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if($template_file == 'page-institutional.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
	if($template_file == 'page-product.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
	if($template_file == 'page-contacts.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
}