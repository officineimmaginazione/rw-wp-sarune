<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta-box-class.php");

if (is_admin()) {
	
	$prefix = "hc-";
	
	// Initiate your meta box
	
	for($i = 0; $i < 4; $i++) {
		
		$carousel = new OI_Meta_Box(
			array(
			"id" => "homepage-carousel-{$i}-mb",
			"title" => "Sezione carosello " . $i,
			"pages" => array("page"),
			"templates" => array("homepage.php"),
			"context" => "normal",
			"priority" => "high",
		));
		
		$carousel->addText($prefix . "btn-txt-" . $i, array("name" => "Testo pulsante", "desc" => "Testo sul pulsante", "gid" => 1));
		$carousel->addImage($prefix . "circle-bg-" . $i, array("name" => "Immagine nel cerchio", "desc" => "Scegli quale immagine comparirà all'interno del cerchio", "gid" => 2));
		$carousel->addImage($prefix . "bg-" . $i, array("name" => "Immagine di sfondo", "desc" => "Scegli un'immagine di sfondo", "gid" => 2));
		$carousel->addText($prefix . "title-" . $i, array("name" => "Titolo", "desc" => "Inserisci un titolo da far comparire sotto l'immagine principale", "gid" => 1));
		
		// Finish Meta Box Declaration 
		$carousel->Finish();
		
	}
	
	/*$prefix = "id_";
	
	// Initiate your meta box
	$details = new OI_Meta_Box(
		array(
		"id" => "homepage_details_mb",
		"title" => "Dettagli",
		"pages" => array("page"),
		"templates" => array("homepage.php"),
		"context" => "normal",
		"priority" => "high",
	));

	$details->addImage($prefix . "img-0", array("name" => "Immagine 1", "desc" => "Scegli quale immagine comparirà a fianco del testo", "gid" => 1));
	$details->addImage($prefix . "img-1", array("name" => "Immagine 2", "desc" => "Scegli quale immagine comparirà a fianco del testo", "gid" => 1));
	$details->addText($prefix . "title", array("name" => "Titolo", "desc" => "Inserisci un titolo per il paragrafo", "gid" => 2));
	$details->addTextList($prefix . "btn", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Pulsante", "gid" => 2));
	$details->addWysiwyg($prefix . "content", array("name" => "Contenuto", "desc" => "Inserisci del contenuto", "gid" => 3));
    $details->addImage($prefix . "mobile-banner", array("name" => "Banner Mobile", "desc" => "Scegli quale immagine comparirà nel banner in formato mobile", "gid" => 4));

	// Finish Meta Box Declaration 
	$details->Finish();
	*/
	
}
