<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta_box_class.php");

if (is_admin()) {
	
	// Immagini aggintive e opzioni immagine in evidenza
	$prefix = "cl_";
	
	$mb = new OI_Meta_Box( array(
		"id" => "contacts_left_mb",
		"title" => "Colonna sinistra",
		"pages" => array("page"),
		"templates" => array("page-contacts.php"),
		"context" => "normal",
		"priority" => "high",
	));
	
	$mb->addWysiwyg($prefix . "content", array("name" => "Contenuto", "desc" => "Inserisci del contenuto nel paragrafo sinistro", "gid" => 1));
	
	$mb->addText($prefix . "facebook", array("name" => "Facebook", "desc" => "Link alla pagina di Facebook", "gid" => 2));
	$mb->addText($prefix . "youtube", array("name" => "Youtube", "desc" => "Link alla pagina di Youtube", "gid" => 2));
	$mb->addText($prefix . "instagram", array("name" => "Instagram", "desc" => "Link alla pagina di Instagram", "gid" => 3));
	$mb->addText($prefix . "twitter", array("name" => "Twitter", "desc" => "Link alla pagina di Twitter", "gid" => 3));
	
	$mb->Finish();
	
	
	// Contenuto pagina
	$prefix = "cr_";
	
	$mb = new OI_Meta_Box( array(
		"id" => "contacts_right_mb",
		"title" => "Colonna destra",
		"pages" => array("page"),
		"templates" => array("page-contacts.php"),
		"context" => "normal",
		"priority" => "high",
	));
	
	$mb->addText($prefix . "overtitle", array("name" => "Occhiello", "desc" => "Aggiungi il testo sopra al titolo della pagina", "gid" => 1));
	$mb->addText($prefix . "description", array("name" => "Descrizione", "desc" => "Aggiungi una piccola descrizione", "gid" => 1));
	$mb->addText($prefix . "shortcode", array("name" => "Shortcode", "desc" => "Inserisci uno shortcode", "gid" => 2));
	
	$mb->Finish();

	
}
