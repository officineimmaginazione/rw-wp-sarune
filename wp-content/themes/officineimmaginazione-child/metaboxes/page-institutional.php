<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta-box-class.php");

if (is_admin()) {
	
	// Immagini aggintive e opzioni immagine in evidenza
	$prefix = "iio-";
	
	$mb = new OI_Meta_Box( array(
		"id" => "institutional-image-options-mb",
		"title" => "Opzioni immagini",
		"pages" => array("page"),
		"templates" => array("page-institutional.php"),
		"context" => "side",
		"priority" => "low",
	));
	
	$mb->addImage($prefix . "extra-featured-image", array("name" => "Immagine in evidenza aggiuntiva", "desc" => "Aggiungi un'immagine in evidenza extra", "gid" => 1));
	$mb->Finish();
	
	
	// Contenuto pagina
	$prefix = "ic-";
	
	$mb = new OI_Meta_Box( array(
		"id" => "institutional-content-mb",
		"title" => "Contenuto",
		"pages" => array("page"),
		"templates" => array("page-institutional.php"),
		"context" => "normal",
		"priority" => "high",
	));
	
	$mb->addText($prefix . "overtitle", array("name" => "Occhiello", "desc" => "Aggiungi un piccolo titolo sopra al titolo principale della pagina", "gid" => 0));
	$mb->addWysiwyg($prefix . "intro", array("name" => "Introduzione", "desc" => "Inserisci del contenuto sotto al titolo", "gid" => 1));
	$mb->addWysiwyg($prefix . "content-left", array("name" => "Contenuto sinitro", "desc" => "Inserisci del contenuto nel paragrafo sinistro", "gid" => 2));
	$mb->addWysiwyg($prefix . "content-right", array("name" => "Contenuto destro", "desc" => "Inserisci del contenuto nel paragrafo sinistro", "gid" => 2));
	$mb->addTextList($prefix . "btn", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Pulsante", "gid" => 3));
	$mb->addTextList($prefix . "btn-right", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Pulsante sulla destra", "gid" => 3));
	$mb->Finish();

	
}
