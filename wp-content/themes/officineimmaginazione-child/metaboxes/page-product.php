<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta_box_class.php");

if (is_admin()) {
	
	// Immagini aggintive e opzioni immagine in evidenza
	$prefix = "pl_";
	
	$mb = new OI_Meta_Box( array(
		"id" => "product_left_mb",
		"title" => "Colonna sinistra",
		"pages" => array("page"),
		"templates" => array("page-product.php"),
		"context" => "normal",
		"priority" => "high",
	));
	
	$mb->addText($prefix . "overtitle", array("name" => "Occhiello", "desc" => "Aggiungi un piccolo titolo sopra al titolo principale della pagina", "gid" => 1));
	$mb->addText($prefix . "title", array("name" => "Titolo", "desc" => "Aggiungi un titolo ", "gid" => 1));
	$mb->addWysiwyg($prefix . "content", array("name" => "Contenuto", "desc" => "Inserisci del contenuto nel paragrafo sinistro", "gid" => 2));
	$mb->Finish();
	
	
	// Contenuto pagina
	$prefix = "pr_";
	
	$mb = new OI_Meta_Box( array(
		"id" => "product_right_mb",
		"title" => "Colonna destra",
		"pages" => array("page"),
		"templates" => array("page-product.php"),
		"context" => "normal",
		"priority" => "high",
	));
	
	$mb->addText($prefix . "title", array("name" => "Titolo", "desc" => "Aggiungi un titolo ", "gid" => 1));
	$mb->addText($prefix . "subtitle", array("name" => "Sottotitolo", "desc" => "Aggiungi un piccolo sottotitolo", "gid" => 1));
	
	$mb->addImage($prefix . "icon_first", array("name" => "Icona", "desc" => "Aggiungi un'immagine", "gid" => 2));
	$mb->addWysiwyg($prefix . "content_first", array("name" => "Paragrafo", "desc" => "Inserisci del contenuto di fianco all'icona", "gid" => 2));
	
	$mb->addImage($prefix . "icon_second", array("name" => "Icona", "desc" => "Aggiungi un'immagine", "gid" => 3));
	$mb->addWysiwyg($prefix . "content_second", array("name" => "Paragrafo", "desc" => "Inserisci del contenuto di fianco all'icona", "gid" => 3));
	
	$mb->addImage($prefix . "icon_third", array("name" => "Icona", "desc" => "Aggiungi un'immagine", "gid" => 4));
	$mb->addWysiwyg($prefix . "content_third", array("name" => "Paragrafo", "desc" => "Inserisci del contenuto di fianco all'icona", "gid" => 4));
	
	$mb->addImage($prefix . "icon_fourth", array("name" => "Icona", "desc" => "Aggiungi un'immagine", "gid" => 5));
	$mb->addWysiwyg($prefix . "content_fourth", array("name" => "Paragrafo", "desc" => "Inserisci del contenuto di fianco all'icona", "gid" => 5));
	
	$mb->addImage($prefix . "icon_fifth", array("name" => "Icona", "desc" => "Aggiungi un'immagine", "gid" => 6));
	$mb->addWysiwyg($prefix . "content_fifth", array("name" => "Paragrafo", "desc" => "Inserisci del contenuto di fianco all'icona", "gid" => 6));
	
	$mb->addImage($prefix . "icon_sixth", array("name" => "Icona", "desc" => "Aggiungi un'immagine", "gid" => 7));
	$mb->addWysiwyg($prefix . "content_sixth", array("name" => "Paragrafo", "desc" => "Inserisci del contenuto di fianco all'icona", "gid" => 7));
	
	$mb->addTextList($prefix . "link", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Pulsante", "gid" => 8));
	$mb->addTextList($prefix . "btn", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Pulsante", "gid" => 8));
	
	$mb->Finish();

	
}
