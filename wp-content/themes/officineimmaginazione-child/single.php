<?php
if(is_singular("products")) :
  get_template_part('singles/product', '');
else :
  get_template_part('singles/post', '');
endif;
