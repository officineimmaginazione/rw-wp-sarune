<?php
/*
Template Name: Pagina istituzionale
*/
?>

<?php while (have_posts()) : the_post(); ?>
	<?php 
	
	$prefix = "iio-";
	$extra_featured_image = get_post_meta(get_the_ID(), $prefix . "extra-featured-image", true);
	
	//print_r($extra_featured_image); exit;

	$prefix = "ic-";
	$content = array (
		"overtitle" => get_post_meta(get_the_ID(), $prefix . "overtitle", true),
		"intro" => get_post_meta(get_the_ID(), $prefix . "intro", true),
		"content_left" => get_post_meta(get_the_ID(), $prefix . "content-left", true),
		"content_right" => get_post_meta(get_the_ID(), $prefix . "content-right", true),
		"link" => array(
			"txt" => get_post_meta(get_the_ID(), $prefix . "btn_txt", true),
			"url" => get_post_meta(get_the_ID(), $prefix . "btn_url", true)
		),
		"link_right" => array(
			"txt" => get_post_meta(get_the_ID(), $prefix . "btn-right_txt", true),
			"url" => get_post_meta(get_the_ID(), $prefix . "btn-right_url", true)
		),
	);
	
	?>
	<main class="institutional-main">
		<section class="container-fluid align-items-center justify-content-center main-body">
      <div class="row">
				<div class="col-12 col-lg-6 body-side-left" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					<?php if(has_post_thumbnail()) : ?>
						<?php if(array_filter($extra_featured_image)) : ?>
							<div class="side-img" style="background-image: url(<?php echo $extra_featured_image["url"] ?>);"></div>
						<?php endif; ?>
					<?php endif; ?>
				</div>
        <div class="d-flex flex-wrap align-items-center col-12 col-lg-6 body-side-right">
					<div class="side-right-content">
						<?php if(array_filter($content)) : ?>
							<?php if($content["overtitle"]) : ?>
								<h5 class="body-overtitle"><?php echo $content["overtitle"]; ?></h5>
							<?php endif; ?>
							<h1 class="body-title"><?php the_title(); ?></h1>
							<div class="row body-content">
								<?php if($content["intro"]) : ?>
									<div class="col-12 body-intro">
										<?php echo do_shortcode($content["intro"]); ?>
									</div>
								<?php endif; ?>
								<?php if($content["content_left"]) : ?>
									<div class="col-12 col-md-6 col-lg-12 col-xl-6">
										<?php echo $content["content_left"]; ?>
									</div>
								<?php endif; ?>
								<?php if($content["content_right"]) : ?>
									<div class="col-12 col-md-6 col-lg-12 col-xl-6">
										<?php echo $content["content_right"]; ?>
									</div>
								<?php endif; ?>
							</div>
							<?php if(array_filter($content["link"]) || array_filter($content["link_right"])) : ?>
								<div class="row">
									<?php if(array_filter($content["link"])) : ?>
										<div class="<?php echo array_filter($content["link_right"]) ? "col-12 col-md-6" : "col-12  text-center"; ?>">
											<a class="btn btn-link p-0" href="<?php echo $content["link"]["url"]; ?>" >
												<i class="la la-long-arrow-right"></i>
												<span><?php echo $content["link"]["txt"]; ?></span>
											</a>
										</div>
									<?php endif; ?>
									<?php if(array_filter($content["link_right"])) : ?>
										<div class="<?php echo array_filter($content["link_right"]) ? "col-12 col-md-6" : "col-12  text-center"; ?>">
											<a class="btn btn-link p-0" href="<?php echo $content["link_right"]["url"]; ?>" >
												<i class="la la-long-arrow-right"></i>
												<span><?php echo $content["link_right"]["txt"]; ?></span>
											</a>
										</div>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
        </div>
      </div>
    </section>
	</main>
<?php endwhile; ?>
<script>
    $(document).ready(function(){
      
			function assignHeight(target) {
				win = $(window);
				header = $(".header");
				footer = $(".footer");
				if(win.height() > 700 && win.width() > 991) {
					$(target).css("height", ((win.height() - header.height())));
				}
				else {
					/*if ((win.height() > 700 && win.width() < 768)) {
						$(target).css("height", (win.height() - (header.outerHeight() + footer.outerHeight())));
					}*/
				}
			}
			
			assignHeight(".body-side-left");
			$(window).resize(function() {
				assignHeight(".body-side-left");
			});
			
			var currentMousePos = { x: -1, y: -1 };
			$(document).mousemove(function(event) {
				if ($(window).width() > 768){
					
					var $wWidth = $(window).innerWidth();
					var $wHeight = $(window).innerHeight();
					var centerX = $wWidth / 2;
					var centerY = $wHeight / 2;
					
					xBgPosition = ((centerX - event.pageX) / 150);
					yBgPosition = ((centerY - event.pageY) / 150);
					$(".side-img").css('background-position', 'calc(50% - ' + xBgPosition + 'px) calc(50% - ' + yBgPosition + 'px)');
					
				}
			});
			
			
    });
</script>