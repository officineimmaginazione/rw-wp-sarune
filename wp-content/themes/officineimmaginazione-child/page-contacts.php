<?php
/*
Template Name: Pagina contatti
*/
?>

<?php while (have_posts()) : the_post(); ?>
	<?php 
	
	$prefix = "cl_";
	$left = array (
		"content" => get_post_meta(get_the_ID(), $prefix . "content", true),
		"facebook" => get_post_meta(get_the_ID(), $prefix . "facebook", true),
		"youtube" => get_post_meta(get_the_ID(), $prefix . "instagram", true),
		"instagram" => get_post_meta(get_the_ID(), $prefix . "instagram", true),
		"twitter" => get_post_meta(get_the_ID(), $prefix . "twitter", true),
	);
	
	//print_r($extra_featured_image); exit;

	$prefix = "cr_";
	$right = array (
		"overtitle" => get_post_meta(get_the_ID(), $prefix . "overtitle", true),
		"description" => get_post_meta(get_the_ID(), $prefix . "description", true),
		"shortcode" => get_post_meta(get_the_ID(), $prefix . "shortcode", true),
	);
	
	?>
	<main class="contacts-main">
		<section class="container-fluid align-items-center justify-content-center main-body">
      <div class="row">
				<div class="d-flex flex-wrap align-items-center justify-content-center col-12 col-lg-6 body-side-left" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">
					<?php if(has_post_thumbnail()) : ?>
						<?php if(array_filter($left)) : ?>
							<div class="side-left-circle">
								<?php if($left["content"]) : ?>
									<div class="circle-content">
										<?php echo $left["content"]; ?>
									</div>
								<?php endif; ?>
								<div class="circle-social">
									<?php if($left["facebook"]) : ?>
										<a href="<?php echo $left["facebook"]; ?>" target="_blank">
											<i class="fab fa-facebook"></i>
										</a>
									<?php endif; ?>
									<?php if($left["youtube"]) : ?>
										<a href="<?php echo $left["youtube"]; ?>" target="_blank">
											<i class="fab fa-youtube"></i>
										</a>
									<?php endif; ?>
									<?php if($left["instagram"]) : ?>
										<a href="<?php echo $left["instagram"]; ?>" target="_blank">
											<i class="fab fa-instagram"></i>
										</a>
									<?php endif; ?>
									<?php if($left["twitter"]) : ?>
										<a href="<?php echo $left["twitter"]; ?>" target="_blank">
											<i class="fab fa-twitter"></i>
										</a>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				</div>
        <div class="d-flex flex-wrap align-items-center col-12 col-lg-6 body-side-right">
					<div class="side-right-content">
						<?php if(array_filter($right)) : ?>
							<?php if($right["overtitle"]) : ?>
								<h5 class="body-overtitle"><?php echo $right["overtitle"]; ?></h5>
							<?php endif; ?>
							<h1 class="body-title"><?php the_title(); ?></h1>
							<?php if($right["description"]) : ?>
								<div class="body-description"><?php echo $right["description"]; ?></div>
							<?php endif; ?>
							<?php if($right["shortcode"]) : ?>
								<div class="form-container"><?php echo do_shortcode($right["shortcode"]); ?></div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
        </div>
      </div>
    </section>
	</main>
<?php endwhile; ?>
<script>
    $(document).ready(function(){
      
			function assignHeight(target) {
				win = $(window);
				header = $(".header");
				footer = $(".footer");
				if(win.height() > 700 && win.width() > 991) {
					$(target).css("height", ((win.height() - header.height())));
				}
			}
			
			assignHeight(".body-side-left");
			$(window).resize(function() {
				assignHeight(".body-side-left");
			});
			
			var currentMousePos = { x: -1, y: -1 };
			$(document).mousemove(function(event) {
				if ($(window).width() > 768){
					
					var $wWidth = $(window).innerWidth();
					var $wHeight = $(window).innerHeight();
					var centerX = $wWidth / 2;
					var centerY = $wHeight / 2;
					
					xBgPosition = ((centerX - event.pageX) / 150);
					yBgPosition = ((centerY - event.pageY) / 150);
					$(".side-img").css('background-position', 'calc(50% - ' + xBgPosition + 'px) calc(50% - ' + yBgPosition + 'px)');
					
				}
			});
			
			
    });
</script>