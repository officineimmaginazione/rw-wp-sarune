<?php
/*
Template Name: Homepage
*/
?>
<?php while (have_posts()) : the_post(); ?>
	<main class="homepage-main">
		<div id="hc-background" class="carousel slide carousel-fade home-carousel" data-ride="carousel" data-pause="false">
			<div class="carousel-inner">
				<?php for($i = 0; $i < 4; $i++) : ?>
					<?php if(get_post_meta($post->ID, 'hc-bg-' . $i, true)) : ?>
						<div class="carousel-item<?php echo ($i==0) ? " active" : "" ?>">
							<?php get_tag_picture(get_post_meta($post->ID, 'hc-bg-' . $i, true)['id']); ?>
						</div>
					<?php endif; ?>
				<?php endfor; ?>
			</div>
		</div>
		<div id="hc-circle" class="carousel slide carousel-fade home-carousel" data-ride="carousel" data-pause="false">
			<div class="carousel-inner">
				<?php for($i = 0; $i < 4; $i++) : ?>
					<?php if(get_post_meta($post->ID, "hc-circle-bg-" . $i, true)) : ?>
						<div class="carousel-item<?php echo ($i==0) ? " active" : "" ?>">
							<?php get_tag_picture(get_post_meta($post->ID, "hc-circle-bg-" . $i, true)["id"]); ?>
						</div>
					<?php endif; ?>
				<?php endfor; ?>
			</div>
			<ol class="carousel-indicators">
				<?php for($i = 0; $i < 4; $i++) : ?>
					<li data-target="#hc-background" data-slide-to="<?php echo $i; ?>" <?php echo ($i==0) ? 'class="active"' : ''; ?>>
						<div class="d-flex flex-wrap align-items-center justify-content-center">
							<?php echo get_post_meta($post->ID, 'hc-btn-txt-' . $i, true); ?>
						</div>
					</li>
				<?php endfor; ?>
			</ol>
			<?php if(has_post_thumbnail()) : ?>
				<div class="carousel-overlay">
					<?php	the_post_thumbnail("full"); ?>
				</div>		
			<?php endif; ?>
		</div>
		<div class="d-flex flex-wrap w-100 home-footer">
			<div id="hc-title" class="carousel slide carousel-fade w-100 home-carousel" data-ride="carousel" data-pause="false">
				<div class="carousel-inner">
					<?php for($i = 0; $i < 4; $i++) : ?>
						<?php if(get_post_meta($post->ID, "hc-title-" . $i, true)) : ?>
							<div class="carousel-item<?php echo ($i==0) ? " active" : "" ?>">
								<h3><?php echo get_post_meta($post->ID, 'hc-title-' . $i, true); ?></h3>
							</div>
						<?php endif; ?>
					<?php endfor; ?>
				</div>
			</div>
		</div>
	</main>
<?php endwhile; ?>

<script>
    jQuery(document).ready(function ($) {
			
			function assignHeight(target) {
				win = $(window);
				header = $(".header");
				footer = $(".footer");
				if(win.height() > 700  && win.width() > 767) {
					$(target).css("height", ((win.height() - header.height()) / 2));
				}
				else {
					if (win.height() > 700 && win.width() < 768) {
						$(target).css("height", (win.height() - (header.outerHeight() + footer.outerHeight())) / 2);
					}
				}
			}

			assignHeight("#hc-background .carousel-item");
			assignHeight(".home-footer");
			$(window).resize(function() {
				assignHeight("#hc-background .carousel-item");
				assignHeight(".home-footer");
			});

			var carousel1 = $('#hc-background').carousel();
			var carousel2 = $('#hc-circle').carousel();
			var carousel3 = $('#hc-title').carousel();
			carousel1.on('slide.bs.carousel', function(event) {
				var to = $(event.relatedTarget).index();
				carousel2.carousel(to)
				carousel3.carousel(to);
				});
				
    });
</script>

