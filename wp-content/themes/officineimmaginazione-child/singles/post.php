<?php while (have_posts()) : the_post(); ?>
	<main class="post-main">
		<div class="container">
			<div class="row align-items-center post-main-content">
				<div class="col-12 col-md-6 col-lg-5 post-side-left">
					<?php echo get_tag_picture(get_post_thumbnail_id()); ?>
				</div>
				<div class="col-12 col-md-6 col-lg-6 ml-auto post-side-right">
					<a class="btn btn-link d-none d-md-inline-block" href="<?php echo get_category_link(1); ?>">
						<i class="la la-long-arrow-left"></i>
						<span><?php _e("Indietro", "officineimmaginazione"); ?></span>
					</a>
					<h4 class="post-date"><?php echo get_the_date("d F Y"); ?></h4>
					<h1 class="post-title"><?php the_title(); ?></h1>
					<div class="post-content"><?php the_content(); ?></div>
					<a class="btn btn-link d-inline-block d-md-none" href="<?php echo get_category_link(1); ?>">
						<i class="la la-long-arrow-left"></i>
						<span><?php _e("Indietro", "officineimmaginazione"); ?></span>
					</a>
				</div>
			</div>
		</div>
	</main>
<?php endwhile; ?>
<script>
    $(document).ready(function(){
      
			function assignHeight(target) {
				win = $(window);
				header = $(".header");
				footer = $(".footer");
				if(win.height() > 700  && win.width() > 767) {
					$(target).css("min-height", ((win.height() - (header.height() + footer.outerHeight()) )));
				}
			}
			
			assignHeight(".post-main-content");
			$(window).resize(function() {
				assignHeight(".post-main-content");
			});
			
			
    });
</script>