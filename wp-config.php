<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'sarune_wp');

/** Nome utente del database MySQL */
define('DB_USER', 'root');

/** Password del database MySQL */
define('DB_PASSWORD', 'root');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>kFl_4=JF*V|~qoeP,rUFd5_cWQ7>&e-d]Bwc>t3gy9n&wrB:|WWksMS/p!>%CZb');
define('SECURE_AUTH_KEY',  'V~ur9&uj}5ocW&Y2My8|X(1yP91ePczj!m(9 CZ0A%B UeFsYkH64}(j%5#*v9h/');
define('LOGGED_IN_KEY',    'S*=p:)U]A.LOeJ)q`LS+6~ad6qsnj9][#Sf>KBx~-@k[Fp&l99kfwN]^F+L2fPFV');
define('NONCE_KEY',        '{OsX6eO_ _6&0n-,H+QO,@>W^7$Sj-OqU>,M/ic$?9Z!&ik*VD)V^_,gyx.767|u');
define('AUTH_SALT',        '$*?6BME.9RW?cY:~^|y!kttC>)ZSMT;6nAe9W~KT!V;N|FutV&;?)$ol$Gp=]QTL');
define('SECURE_AUTH_SALT', 'Sz&G@BH=OICY7}*j}a%;[CKh[OdI$8/r(M,*Cb6`_cO9=0=#KBtXiRz5SeBZ0#;[');
define('LOGGED_IN_SALT',   'tARMf$6l/3r(O)kXb^PJ(I2,4*!w{9v QSdW7]-I25R3^_(%T@{X8g<d]jH}kDo[');
define('NONCE_SALT',       '.PnbpK &w<x6f_xg8Pyv>(Ya#&1`Oo-Q/FmxL/k,~|Brq<r:^+LBUKdDx| {9bOk');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');